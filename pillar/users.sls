# format data like the following...
#
# users:
#
#  tusername:
#    fullname: Test Username
#    uid: 1007
#    gid: 1007
#    groups:
#      - sudo
#      - ops
#    crypt: $password-hash-sha512-prefered
#    pub_ssh_keys:
#      - ssh-rsa list-of-public-keys tusername-sm
#
#  anotheruser: ... snipped ...

users:
  abenedict:
  fullname: Alan Benedict
  uid: 1000
  gid: 1000
  groups:
    -wheel
  crypt: $6$xKcfr/eJbqZjaJsG$VNbHCjZDVbvkbLGfwlm2Cb382e1gZF1eFM5ArWMMTuywsbMAbPALEDgcKi8kAQyyMJ/kCUNhE0AYSpkK6E/2i1