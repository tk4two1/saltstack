sudoers_file:
  file.managed:
    - name: /etc/sudoers
    - source: salt://base/sudoers/sudoers
    - user: root
    - group: root
    - mode: 0440