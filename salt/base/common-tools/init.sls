install_common_tools:
  pkg.installed:
    - open-vm-tools
    - wget
    - ncdu
    - htop
    - bash-completion
    - mc
    